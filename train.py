import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout
from keras.layers import Flatten
from keras.layers.convolutional import Convolution2D
from keras.layers.convolutional import MaxPooling2D
from keras.utils import to_categorical
from keras import backend as K
import numpy
import pandas as pd
import sys
import matplotlib.pyplot as plt

def print_train_data():
    # MNISTデータの表示
    W = 3  # 横に並べる個数
    H = 3   # 縦に並べる個数
    fig = plt.figure(figsize=(H, W))
    fig.subplots_adjust(left=0, right=1,\
        bottom=0, top=1.0, hspace=0.05, wspace=0.05)
    for i in range(W*H):
        ax = fig.add_subplot(H, W, i + 1,\
            xticks=[], yticks=[])
        ax.imshow(X_train[i].reshape((28, 28)), cmap='gray')

    plt.show()

(X_train,Y_train) , (X_test,Y_test) = mnist.load_data()
#print_train_data()
#sys.exit()

X_train = X_train.reshape(X_train.shape[0],28,28,1).astype('float32')
X_test = X_test.reshape(X_test.shape[0],28,28,1).astype('float32')

Y_train = to_categorical(Y_train)
Y_test = to_categorical(Y_test)

X_train = X_train / 255
X_test = X_test / 255

input_shape = X_train.shape[1:]
def create_model():
    num_classes = 10
    model = Sequential()
    model.add(Convolution2D(32,kernel_size=(3,3),activation='relu',input_shape=input_shape))
    model.add(Convolution2D(64,(3,3),activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(256,activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(num_classes,activation='softmax'))
    model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
    return model

model = create_model()

model.fit(X_train,Y_train,validation_data=(X_test,Y_test),epochs=10,batch_size=200,verbose=2)

model.save('model2.h5')
model.save_weights('weights2.h5')
model.evaluate(X_test,Y_test,verbose=1)